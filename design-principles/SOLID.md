# SOLID Design principles

* Single Responsibility Principle
* Open/Closed Principle
* Liskov Substitution Principle
* Interface Segregation Principle
* Dependency Inversion

# Single Responsibility Principle
## "A class should have one, and only one, reason to change."

**Q:** What are the benefits of using this principle?

Software requirements change over time and alongside with them the responsibilities of your classes. The more responsibilities a single class have the more reasons to change. That makes code hard to maintain and prone to unexpected side-effects. Furthermore that affects all the classes and components that depend on the changed class.

When a class/component/microservice has only one resposibility it's easier to explain and understand.

**Q:** How to know if I follow the Single Responsibility Principle?

Ask yourself the following question: What is the responsibility of my class/component/microservice? If your answer includes the word "and", you are most likely breaking the Single Responsibility Principle.



# Open/Closed Principle
## "Software entities (classes, modules, functions, etc.) should be open for extension, but closed for modification."

**Q:** What are the benefits of using this principle?

* It will allow you to add new functionality without the need of changing existing code or adapt all of the depending classes (closed for modification).
* Introduces addition level of abstraction which enables loose coupling (open for extension).

The idea behind this principle is to allow different implementations that you could easily substitude without changing the code that uses them. And because this is just principle it doesn't provide specific solution on that problem because it may vary depending on the context and on the programming language.

However, there are some general patterns that could help us achieve this:
* Program by Interface not by Implementation
* Strategy Pattern

# Liskov Substitution Principle



# Interface Segregation Principle



# Dependency Inversion