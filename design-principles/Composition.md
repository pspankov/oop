It describes a class that references one or more objects of other classes in instance variables.

> **Q:** What are the benefits of using this concept?

* The code is cleaner
* You can reuse existing code
* You can change the implementation without adapting the external clients