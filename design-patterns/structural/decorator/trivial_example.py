"""
Attach additional responsibilities to an object dynamically. Decorators
provide a flexible alternative to subclassing for extending
functionality.
"""

from abc import ABC, abstractmethod


class Component(ABC):
    """
    Define the interface for objects that can have responsibilities
    added to them dynamically.
    """

    @abstractmethod
    def method_a(self):
        pass

    @abstractmethod
    def method_b(self):
        pass

    """ other methods """


class ConcreteComponent(Component):
    """
    Define an object to which additional responsibilities can be
    attached.
    """

    def method_a(self):
        pass

    def method_b(self):
        pass

    """ other methods """


class Decorator(Component, ABC):
    """
    Maintain a reference to a Component object and define an interface
    that conforms to Component's interface.
    """

    def __init__(self, component):
        self._component = component

    @abstractmethod
    def method_a(self):
        pass

    @abstractmethod
    def method_b(self):
        pass


class ConcreteDecoratorA(Decorator):
    """
    Add responsibilities to the component.
    """

    def method_a(self):
        # ...
        self._component.method_a()
        # ...

    def method_b(self):
        # ...
        self._component.method_b()
        # ...

    def some_new_behavior(self):
        pass


class ConcreteDecoratorB(Decorator):
    """
    Add responsibilities to the component.
    """

    new_state = None

    def method_a(self):
        # ...
        self._component.method_a()
        # ...

    def method_b(self):
        # ...
        self._component.method_b()
        # ...


def main():
    concrete_component = ConcreteComponent()
    concrete_decorator_a = ConcreteDecoratorA(concrete_component)
    concrete_decorator_b = ConcreteDecoratorB(concrete_decorator_a)
    concrete_decorator_b.method_a()


if __name__ == "__main__":
    main()
