"""
The code bellow implements the following problem:
We have a coffee shop and there are several types of drinks with different cost.
Also we have multiple extras (components) that can be added to each beverage with a fixed price (i.e whipped cream, milk, etc).
Using the decorator pattern we can dynamically add as many components as we like to a selected drink and get a
calculated total price on everything.
"""

from abc import ABC, abstractmethod, abstractproperty


class Drink(ABC):
    """
    Drink acts as our abstract base component that can have extras added to it dynamically.
    """

    @abstractmethod
    def get_description(self):
        pass

    @abstractmethod
    def get_cost(self):
        pass


class DrinkComponent(Drink):
    """
    Drink component acts as our abstract decorator.
    """

    def __init__(self, drink):
        self.drink = drink


"""
Let's implements some concrete drinks
"""


class Espresso(Drink):
    def get_description(self):
        return "Espresso"

    def get_cost(self):
        return 1.99


class Tea(Drink):
    def get_description(self):
        return "Tea"

    def get_cost(self):
        return 1.29


class Chocolate(Drink):
    def get_description(self):
        return "Chocolate"

    def get_cost(self):
        return 3.49


"""
Let's implements some concrete decorator components
"""


class Whipped(DrinkComponent):
    def get_description(self):
        return self.drink.get_description() + ", Whipped"

    def get_cost(self):
        return 0.20 + self.drink.get_cost()


class FoamedMilk(DrinkComponent):
    def get_description(self):
        return self.drink.get_description() + ", Foamed Milk"

    def get_cost(self):
        return 0.30 + self.drink.get_cost()


class WhippedMilk(DrinkComponent):
    def get_description(self):
        return self.drink.get_description() + ", Whipped Milk"

    def get_cost(self):
        return 0.30 + self.drink.get_cost()


class Honey(DrinkComponent):
    def get_description(self):
        return self.drink.get_description() + ", Honey"

    def get_cost(self):
        return 0.10 + self.drink.get_cost()


def example():
    # we order just tea
    drink1 = Tea()
    print(drink1.get_description())
    print("{0:.2f}".format(drink1.get_cost()))

    # we order espresso and add whipped and foamed milk to it
    drink2 = Espresso()
    drink2 = Whipped(drink2)
    drink2 = FoamedMilk(drink2)
    print(drink2.get_description())
    print("{0:.2f}".format(drink2.get_cost()))

if __name__ == "__main__":
    example()
