# Proxy design pattern #

Proxy means 'in place of', 'representing' or the authority to represent someone else. 
It's also called wrapper, surrogate, handle.

## Problem ##

You need to support resource-hungry objects, and you do not want to instantiate such objects 
unless and until they are actually requested by the client.

## Use cases ##

The Proxy pattern can be typically used when you need to extend another object's
functionalities, specifically the following:

* To control access to another object, for example, for security reasons.
* To log all calls to Subject with its parameters.
* To connect to Subject, which is located on remote machine or another
address space. A Proxy has an interface of a remote object but also handles
the connection routine that is transparent to the caller.
* To instantiate a heavy object only when it is really needed. It can also
cache a heavy object (or part of it).
* To temporarily store some calculation results before returning to multiple
clients that can share these results.
* To count references to an object

Examples:
* *A bank's cheque or credit card is a proxy for what is in our bank account. 
    It can be used in place of cash, and provides a means of accessing that cash when 
    required.*
* *A company or corporate used to have a proxy which restricts few site 
    access. The proxy first checks the host you are connecting to, if it is not a 
    part of restricted site list, then it connects to the real internet.*

##### Sources #####

* https://sourcemaking.com/design_patterns/proxy
* http://www.oodesign.com/proxy-pattern.html
* https://stackoverflow.com/questions/28053847/usage-of-proxy-design-pattern
