from abc import ABC, abstractmethod

"""
The code below shows the Image interface representing the Subject. 
The interface has a single method showImage() that the Concrete Images must implement to render an image to screen.
"""


class Image(ABC):
    """Subject interface"""

    @abstractmethod
    def show_image(self):
        pass


class RealImage(Image):
    """ Real subject """

    def __init__(self, image_file_path):
        self.image_file_path = image_file_path
        self.load_image_from_disk()

    def load_image_from_disk(self):
        # load Image from disk into memory
        # this is heavy and costly operation
        print("Loading {}".format(self.image_file_path))

    def show_image(self):
        print("Displaying {}".format(self.image_file_path))


class ImageProxy(Image):
    """ Proxy """

    # reference to the real subject
    real_image = None

    # proxy data
    image_file_path = None

    def __init__(self, image_file_path):
        self.image_file_path = image_file_path

    def show_image(self):
        # create the real image object only when the image is required to be shown

        # also could introduce caching here if the same images is displayed several times
        if not self.real_image:
            self.real_image = RealImage(self.image_file_path)

        self.real_image.show_image()


def example():
    # assuming that the user selects a folder that has 3 images
    # create the 3 images
    high_res_image1 = ImageProxy("sample/veryHighResPhoto1.jpeg")
    high_res_image2 = ImageProxy("sample/veryHighResPhoto2.jpeg")
    high_res_image3 = ImageProxy("sample/veryHighResPhoto3.jpeg")

    # assume that the user clicks on Image one item in a list
    # this would cause the program to call showImage() for that image only
    # note that in this case only image one was loaded into memory
    high_res_image1.show_image()

    # consider using the high resolution image object directly
    high_res_image_no_proxy_1 = RealImage("sample/veryHighResPhoto1.jpeg")
    high_res_image_no_proxy_2 = RealImage("sample/veryHighResPhoto2.jpeg")
    high_res_image_no_proxy_3 = RealImage("sample/veryHighResPhoto3.jpeg")

    # assume that the user selects image two item from images list
    high_res_image_no_proxy_2.show_image()

    # note that in this case all images have been loaded into memory
    # and not all have been actually displayed
    # this is a waste of memory resources


if __name__ == "__main__":
    example()
