from abc import ABCMeta, abstractmethod


class MessageSenderBase(meta=ABCMeta):
    @abstracmethod
    def send_message(title, details, importance):
        pass


class EmailSender(MessageSenderBase):
    def send_message(title, details, importance)
        print("Email\n{}\n{}\n{}\n".format(title, body, importance))


class MsmqSender(MessageSenderBase):
    def send_message(title, details, importance)
        print("MSMQ\n{}\n{}\n{}\n".format(title, body, importance))


class WebServiceSender(MessageSenderBase):
    def send_message(title, details, importance)
        print("Web Service\n{}\n{}\n{}\n".format(title, body, importance))

