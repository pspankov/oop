# Bridge Pattern

**Bridge** is a structural design pattern that divides one or more classes into two separate hierarchies - abstraction and implementation, allowing them to be changed independently of each other.

or Definition from GoF, Design Patterns book:
> Decouples an abstraction from its implementation so that the two can vary independently.

## Examples

**High-level example:**

Bridge pattern is about preferring composition over inheritance. Implementation details are pushed from a hierarchy to another object with a separate hierarchy.

When:
```
                   ----Shape---
                  /            \
         Rectangle              Circle
        /         \            /      \
BlueRectangle  RedRectangle BlueCircle RedCircle
```
Refactor to:
```

          ----Shape---   ----- Bridge -----   Color
         /            \                       /   \
Rectangle(Color)   Circle(Color)           Blue   Red
```

**Fictional functional example**

[Example 1 - Shapes and Colors](bridge_example_1.py)
