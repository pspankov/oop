# Mediator Design Pattern

Mediator pattern adds a third party object (called mediator) to control 
the interaction between two objects (called colleagues).
It helps reduce the coupling between the classes communicating with each other.

## Problem ##
We want to design reusable components, but dependencies between the potentially 
reusable pieces demonstrates the "spaghetti code" phenomenon 
(trying to scoop a single serving results in an "all or nothing clump").

## Use cases ##
* A set of objects communicate in well-defined but complex ways. 
The resulting interdependencies are unstructured and difficult to understand.
* Reusing an object is difficult because it refers to and communicates with many other objects.
* A behavior that’s distributed between several classes 
should be customizable without a lot of sub-classing

Examples:

* GUI implementation. In GUI we have a lot of different components that have different 
functionality and they don't know about each other but have to work together.
That's when we can take advantage of mediator pattern.
(e.g. a form which have several input components and a submit button in order to be 
able to click the submit button and proceed all of the input components have to have their 
data validated)
* Chat - again we can use mediator pattern to centralize the communication between 
members of a chat room.

##### Sources #####
* http://en.proft.me/2017/02/17/mediator-pattern-java-and-python/
* https://sourcemaking.com/design_patterns/mediator
* http://www.oodesign.com/mediator-pattern.html
