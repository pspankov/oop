"""
We have Chat app concept where we can add as many user instances as we like.
Every user can send messages through the mediator and it will make sure that all members of that chat room
will receive the message without the need of all of them knowing the existence of each other.
"""

from abc import ABC, abstractmethod

class UserBase(ABC):
    """
    The abstract base class of the "colleague" classes
    """
    def __init__(self, med, name):
        self.mediator = med
        self.name = name

    @abstractmethod
    def send(self, msg):
        pass

    @abstractmethod
    def receive(self, msg):
        pass


class ChatRoom():
    """
    This is the concrete implementation of the mediator class.
    It will manage the communication between all of the users (colleague classes).
    """
    def __init__(self):
        self.users = []

    def add_user(self, user):
        self.users.append(user)

    def send_message(self, msg, user):
        for u in self.users:
            if u != user:
                u.receive(msg)


class User(UserBase):
    """
    Concrete implementation of the colleague class
    """
    def send(self, msg):
        print(self.name + ": Sending Message: " + msg)
        self.mediator.send_message(msg, self)

    def receive(self, msg):
        print(self.name + ": Received Message: " + msg)


if __name__ == '__main__':
    mediator = ChatRoom()
    user1 = User(mediator, "John")
    user2 = User(mediator, "Lisa")
    user3 = User(mediator, "Maria")
    user4 = User(mediator, "David")
    mediator.add_user(user1)
    mediator.add_user(user2)
    mediator.add_user(user3)
    mediator.add_user(user4)

    user1.send("Hi All")